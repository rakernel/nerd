# NERD Theme
Forked from Casper!
For Ghost Only :(
Just some little changes :)

## Changes
- 删去Share（需要的请参见官方Casper）
- 首页也能显示图片了
- 配色方案参见了[Silent](https://github.com/s4iko/Silent-theme)
- 全局删除`border-radius`
- Disqus支持

## ChangeLog
|Time|Version|Changes|
|----+-------+-------|
|07/05|1.2.4#000|init|

_版本的命名方式为`Casper版本`#`NERD版本`_

## License
See `LICENSE`